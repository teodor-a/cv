---
title: "Teodor Alling CV"
---

# Intro
Ecologist + bioinformatician passionate about writing code and learning about all sorts of living systems.

Skills:

R - Python - Scientific reasoning - Workflows - Teaching/communication - Scientific writing


# Projects I have worked on
- **Spatial transcriptomics data analysis** As part of my master's thesis project, I made scripts for the analysis of spatial transciptomic data from the Nanostring GeoMx platform. [Link to repo](https://gitlab.com/teodor-a/spatial-analysis)
- **scRNA-seq data analysis** The other half of my master thesis involved analysing scRNA-seq data from the 10x Chromium platform. [Link to repo](https://gitlab.com/teodor-a/scrna-analysis)
- **pop-pop** An Rshiny app that uses your genetic variant information and your sample location information to draw a map with admixture plots. If you supply a migration matrix, arrows will be drawn on the map visualising the migration paths. Created as part of the course BINP29 at Lund University. [Link to repo](https://gitlab.com/teodor-a/pop-pop)
- **De novo genome assembly** A workflow for the de novo assembly and evaluation of a PacBio SMRT-sequenced yeast genome. Written as part of a project for the course BINP29, Lund University. [Link to repo](https://gitlab.com/teodor-a/denovo-assembly)
- **PowerMVM** What started as (part of) a project conducted within the [Molecular Ecology and Evolution Lab at Lund University](https://portal.research.lu.se/en/organisations/molecular-ecology-and-evolution-lab) has since been worked on in my spare time to become a series of scripts used to download data from the online database Miljödata MVM, via its API. [Link to repo](https://gitlab.com/teodor-a/powermvm)
- **Mini R tools** Where I put my specific but useful R snippets. [Link to repo](https://gitlab.com/teodor-a/minirtools)
- **Mini bioinfo tools** Where I put my specific but useful python and R scripts specifically related to bioiniformatics. [Link to repo](https://gitlab.com/teodor-a/minibioinfotools)
- **Misc tools** Where I put other mini programs that are just useful to me in my daily computer usage. [Link to repo](https://gitlab.com/teodor-a/misc-tools)


# Contact
Please visit my LinkedIn, ResearchGate, or ORCID for contact details:
<br>
<a href="https://linkedin.com/in/teodoralling">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/240px-LinkedIn_logo_initials.png" alt="Visit my LinkedIn" width="64" height="auto">
</a>
<a href="https://www.researchgate.net/profile/Teodor-Alling">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/ResearchGate_icon_SVG.svg/240px-ResearchGate_icon_SVG.svg.png" alt="Visit my ResearchGate" width="64" height="auto">
</a>
<a href="https://orcid.org/0000-0003-1680-5666">
    <img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" alt="Visit my ORCID" width="64" height="auto">
</a>
<br>
(Clicky)

